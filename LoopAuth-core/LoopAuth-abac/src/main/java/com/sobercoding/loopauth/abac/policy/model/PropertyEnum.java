package com.sobercoding.loopauth.abac.policy.model;

import java.util.function.Supplier;

public enum PropertyEnum {
    ACTION,
    CONTEXTUAL,
    RES_OBJECT,
    SUBJECT;
}